# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-06-11 22:09+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/blending_modes/modulo.rst:1
msgid "Page about the modulo blending modes in Krita:"
msgstr "Sida om restblandningslägen i Krita."

#: ../../reference_manual/blending_modes/modulo.rst:10
#: ../../reference_manual/blending_modes/modulo.rst:14
#: ../../reference_manual/blending_modes/modulo.rst:47
#: ../../reference_manual/blending_modes/modulo.rst:51
msgid "Modulo"
msgstr "Rest"

#: ../../reference_manual/blending_modes/modulo.rst:16
msgid ""
"Modulo modes are special class of blending modes which loops values when the "
"value of channel blend layer is less than the value of channel in base "
"layers. All modes in modulo modes retains the absolute of the remainder if "
"the value is greater than the maximum value or the value is less than "
"minimum value. Continuous modes assume if the calculated value before modulo "
"operation is within the range between a odd number to even number, then "
"values are inverted in the end result, so values are perceived to be wave-"
"like."
msgstr ""
"Restlägen är en särskild klass av blandningslägen som upprepar värden när "
"värdet på kanalens blandningslager är mindre än värdet på kanalerna i "
"baslager. Alla lägen i restlägen behåller restens absolutvärde om värdet är "
"större än det maximala värdet eller värdet är mindre än det minimala värdet. "
"Kontinuerliga lägen antar att om det beräknade värdet innan restoperationen "
"är inom intervallet mellan ett udda tal till ett jämnt tal, så är värdena "
"inverterade i slutresultatet, så värden betraktas som vågliknande."

#: ../../reference_manual/blending_modes/modulo.rst:18
msgid ""
"Furthermore, this would imply that modulo modes are beneficial for abstract "
"art, and manipulation of gradients."
msgstr ""
"Dessutom skulle det innebära att rester är nyttiga för abstrakt konst, och "
"manipulation av toningar."

#: ../../reference_manual/blending_modes/modulo.rst:20
#: ../../reference_manual/blending_modes/modulo.rst:24
msgid "Divisive Modulo"
msgstr "Dividerande rest"

#: ../../reference_manual/blending_modes/modulo.rst:26
msgid ""
"First, Base Layer is divided by the sum of blend layer and the minimum "
"possible value after zero. Then, performs a modulo calculation using the "
"value found with the sum of blend layer and the minimum possible value after "
"zero."
msgstr ""
"Först divideras baslagret med summan av blandningslager och det minsta "
"möjliga värdet efter noll. Därefter utförs en restberäkning med användning "
"av det beräknade värdet med summan av blandningslager och det minsta möjliga "
"värdet efter noll."

#: ../../reference_manual/blending_modes/modulo.rst:31
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:31
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Divisive Modulo**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **Dividerande "
"rest**."

#: ../../reference_manual/blending_modes/modulo.rst:33
#: ../../reference_manual/blending_modes/modulo.rst:38
msgid "Divisive Modulo - Continuous"
msgstr "Dividerande rest - kontinuerlig"

#: ../../reference_manual/blending_modes/modulo.rst:40
msgid ""
"First, Base Layer is divided by the sum of blend layer and the minimum "
"possible value after zero. Then, performs a modulo calculation using the "
"value found with the sum of blend layer and the minimum possible value after "
"zero. As this is a continuous mode, anything between odd to even numbers are "
"inverted."
msgstr ""
"Först divideras baslagret med summan av blandningslager och det minsta "
"möjliga värdet efter noll. Därefter utförs en restberäkning med användning "
"av det beräknade värdet med summan av blandningslager och det minsta möjliga "
"värdet efter noll. Eftersom det är ett kontinuerligt läge,  inverteras allt "
"mellan udda och jämna tal."

#: ../../reference_manual/blending_modes/modulo.rst:45
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Continuous_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Divisive_Modulo_Continuous_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:45
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Divisive Modulo - "
"Continuous**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **Dividerande "
"rest - kontinuerlig**."

#: ../../reference_manual/blending_modes/modulo.rst:53
msgid ""
"Performs a modulo calculation using the sum of blend layer and the minimum "
"possible value after zero."
msgstr ""
"Utför en restberäkning med användning av summan av blandningslager och det "
"minsta möjliga värdet efter noll."

#: ../../reference_manual/blending_modes/modulo.rst:58
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:58
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo**."
msgstr "Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **Rest**."

#: ../../reference_manual/blending_modes/modulo.rst:60
#: ../../reference_manual/blending_modes/modulo.rst:64
msgid "Modulo - Continuous"
msgstr "Rest - kontinuerlig"

#: ../../reference_manual/blending_modes/modulo.rst:66
msgid ""
"Performs a modulo calculation using the sum of blend layer and the minimum "
"possible value after zero. As this is a continuous mode, anything between "
"odd to even numbers are inverted."
msgstr ""
"Utför en restberäkning med användning av summan av blandningslager och det "
"minsta möjliga värdet efter noll. Eftersom det är ett kontinuerligt läge,  "
"inverteras allt mellan udda och jämna tal."

#: ../../reference_manual/blending_modes/modulo.rst:71
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Continuous_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Continuous_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:71
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo - "
"Continuous**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **Rest - "
"kontinuerlig**."

#: ../../reference_manual/blending_modes/modulo.rst:73
#: ../../reference_manual/blending_modes/modulo.rst:77
msgid "Modulo Shift"
msgstr "Restskiftning"

#: ../../reference_manual/blending_modes/modulo.rst:79
msgid ""
"Performs a modulo calculation with the result of the sum of base and blend "
"layer by the sum of blend layer with the minimum possible value after zero."
msgstr ""
"Utför en restberäkning med resultatet av summan av bas och blandningslager "
"av summan av blandningslager med det minsta möjliga värdet efter noll."

#: ../../reference_manual/blending_modes/modulo.rst:85
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:85
msgid "Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo Shift**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **Restskiftning**."

#: ../../reference_manual/blending_modes/modulo.rst:87
#: ../../reference_manual/blending_modes/modulo.rst:91
msgid "Modulo Shift - Continuous"
msgstr "Restskiftning - kontinuerlig"

#: ../../reference_manual/blending_modes/modulo.rst:93
msgid ""
"Performs a modulo calculation with the result of the sum of base and blend "
"layer by the sum of blend layer with the minimum possible value after zero.  "
"As this is a continuous mode, anything between odd to even numbers are "
"inverted."
msgstr ""
"Utför en restberäkning med resultatet av summan av bas och blandningslager "
"av summan av blandningslager med det minsta möjliga värdet efter noll. "
"Eftersom det är ett kontinuerligt läge,  inverteras allt mellan udda och "
"jämna tal."

#: ../../reference_manual/blending_modes/modulo.rst:98
msgid ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Continuous_Gradient_Comparison.png"
msgstr ""
".. image:: images/blending_modes/modulo/"
"Blending_modes_Modulo_Shift_Continuous_Gradient_Comparison.png"

#: ../../reference_manual/blending_modes/modulo.rst:98
msgid ""
"Left: **Base Layer**. Middle: **Blend Layer**. Right: **Modulo Shift - "
"Continuous**."
msgstr ""
"Vänster: **Baslager**. Mitten: **Blandningslager**. Höger: **Restskiftning - "
"kontinuerlig**."
