# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-09 03:10+0200\n"
"PO-Revision-Date: 2019-07-09 11:30+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Tabletes Huion krita trig BPaste pastebin Pen Wintab\n"
"X-POFile-SpellExtra: Reddit Update Mac net ref Identity Pasteboard bpaste\n"
"X-POFile-SpellExtra: Imgur ffmpeg faqresetkritaconfiguration\n"
"X-POFile-SpellExtra: listsupportedtablets Krita Wacom drminw Pastebin\n"
"X-POFile-SpellExtra: thekritacommunity SO freenote Ink imgur kra guilabel\n"
"X-POFile-SpellExtra: menuselection FFmpeg\n"

#: ../../contributors_manual/user_support.rst:1
msgid "Introduction to user support."
msgstr "Introdução ao apoio ao utilizador."

#: ../../contributors_manual/user_support.rst:20
msgid "Introduction to User Support"
msgstr "Introdução ao Apoio ao Utilizador"

#: ../../contributors_manual/user_support.rst:35
msgid "Contents"
msgstr "Conteúdo"

#: ../../contributors_manual/user_support.rst:38
msgid "Tablet Support"
msgstr "Suporte para Tabletes"

#: ../../contributors_manual/user_support.rst:40
msgid ""
"The majority of help requests are about pen pressure and tablet support in "
"general."
msgstr ""
"A maioria dos pedidos de ajuda são acerca do suporte para tabletes e da "
"pressão da caneta, de um modo geral."

#: ../../contributors_manual/user_support.rst:44
msgid "Quick solutions"
msgstr "Soluções rápidas"

#: ../../contributors_manual/user_support.rst:46
msgid ""
"On Windows: reinstall your driver (Windows Update often breaks tablet driver "
"settings, reinstallation helps)."
msgstr ""
"No Windows: reinstale o seu controlador (o Windows Update normalmente "
"danifica as configurações do controlador da tablete, sendo que uma "
"reinstalação poderá ajudar)."

#: ../../contributors_manual/user_support.rst:48
msgid ""
"Change API in :menuselection:`Settings --> Configure Krita --> Tablet "
"Settings` (for some devices, especially N-trig ones, Windows Ink work "
"better, for some it's Wintab)."
msgstr ""
"Mude a API em :menuselection:`Configuração --> Configurar o Krita --> "
"Configuração da Tablete` (para alguns dispositivos, especialmente os N-trig, "
"o Windows Ink funciona melhor - para outros é o Wintab)."

#: ../../contributors_manual/user_support.rst:50
msgid ""
"On Windows, Wacom tablets: if you get straight lines at the beginnings of "
"the strokes, disable/minimize \"double-click distance\" in Wacom settings."
msgstr ""
"No Windows com tabletes Wacom: se obter linhas rectas nos inícios dos "
"traços, desactive/minimize a \"distância do duplo-click\" na configuração do "
"Wacom."

#: ../../contributors_manual/user_support.rst:53
msgid "Gathering information"
msgstr "A recolher informações"

#: ../../contributors_manual/user_support.rst:55
msgid "Which OS do you use?"
msgstr "Qual o SO que usa?"

#: ../../contributors_manual/user_support.rst:57
msgid "Which tablet do you have?"
msgstr "Qual a tablete que tem?"

#: ../../contributors_manual/user_support.rst:59
msgid "What is the version of the tablet driver?"
msgstr "Qual é a versão do controlador da tablete?"

#: ../../contributors_manual/user_support.rst:61
msgid ""
"Please collect Tablet Tester (:menuselection:`Settings --> Configure Krita --"
"> Tablet Settings`) output, paste it to `Pastebin <https://pastebin.com/>`_ "
"or similar website and give us a link."
msgstr ""
"Por favor use o resultado do Teste da Tablete (:menuselection:`Configuração "
"--> Configurar o Krita --> Configuração da Tablete`), cole-o no `Pastebin "
"<https://pastebin.com/>`_ ou noutra página Web semelhante e envie-nos a "
"ligação."

#: ../../contributors_manual/user_support.rst:65
msgid "Additional information for supporters"
msgstr "Informação adicional de suporte"

#: ../../contributors_manual/user_support.rst:67
msgid ""
"Except for the issue with beginnings of the strokes, Wacom tablets usually "
"work no matter the OS."
msgstr ""
"Exceptuando o problema com os inícios dos traços, as tabletes da Wacom "
"normalmente funcionam independentemente do SO."

#: ../../contributors_manual/user_support.rst:69
msgid ""
"Huion tablets should work on Windows and on Linux, on Mac there might be "
"issues."
msgstr ""
"As tabletes da Huion devem funcionar no Windows e no Linux; no Mac poderão "
"ocorrer problemas."

#: ../../contributors_manual/user_support.rst:71
msgid "XP-Pen tablets and other brands can have issues everywhere."
msgstr ""
"As tabletes XP-Pen e as outras marcas poderão ter problemas em todo o lado."

#: ../../contributors_manual/user_support.rst:73
msgid ""
"If someone asks about a tablet to buy, generally a cheaper Wacom or a Huion "
"are the best options as of 2019, if they want to work with Krita. :ref:"
"`list_supported_tablets`"
msgstr ""
"Se alguém perguntar sobre uma tablete para comprar, normalmente uma uma "
"Wacom barata ou uma Huion serão as melhores opções em 2019, caso queiram "
"trabalhar com o Krita. :ref:`list_supported_tablets`"

#: ../../contributors_manual/user_support.rst:75
msgid ""
"`Possibly useful instruction in case of XP-Pen tablet issues <https://www."
"reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."
msgstr ""
"`Possíveis instruções úteis no caso de problemas com tabletes XP Pen "
"<https://www.reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."

#: ../../contributors_manual/user_support.rst:79
msgid "Animation"
msgstr "Animação"

#: ../../contributors_manual/user_support.rst:81
msgid ""
"Issues with rendering animation can be of various shapes and colors. First "
"thing to find out is whether the issue happens on Krita's or FFmpeg's side "
"(Krita saves all the frames, then FFmpeg is used to render a video using "
"this sequence of images). To learn that, instruct the user to render as "
"\"Image Sequence\". If the image sequence is correct, FFmpeg (or more often: "
"render options) are at fault. If the image sequence is incorrect, either the "
"options are wrong (if for example not every frame got rendered), or it's a "
"bug in Krita."
msgstr ""
"As questões com a animação do desenho poderão ter vários feitios. A primeira "
"coisa a descobrir é se o problema acontece do lado do Krita ou do "
"'ffmpeg' (o Krita grava todas as imagens e depois o FFmpeg é usado para "
"desenhar um vídeo com essa sequência de imagens). Para aprender mais, diga "
"ao utilizador para desenhar como \"Sequência de Imagens\". Se a sequência "
"estiver correcta, o 'ffmpeg' (ou mais frequentemente: as opções de desenho) "
"são os responsáveis. Se a sequência de imagens estiver incorrecta, ou as "
"opções estão erradas (se por exemplo nem todas as imagens foram desenhadas) "
"ou então será um erro no Krita."

#: ../../contributors_manual/user_support.rst:85
msgid ""
"If the user opens the Log Viewer docker, turns on logging and then tries to "
"render a video, Krita will print out the whole ffmpeg command to Log Viewer "
"so it can be easily investigated."
msgstr ""
"Se o utilizador abrir a área do Visualizador do Registo, activar o registo "
"de eventos e depois tentar desenhar um vídeo, o Krita irá imprimir o comando "
"completo do 'ffmpeg' para o Visualizador de Registo, para que possa "
"investigar facilmente o mesmo."

#: ../../contributors_manual/user_support.rst:87
msgid ""
"There is a log file in the directory that user tries to render to. It can "
"contain information useful to investigation of the issue."
msgstr ""
"Existe um ficheiro de registo na pasta onde o utilizador irá tentar gerar o "
"desenho. Poderá conter informações úteis para a investigação do problema."

#: ../../contributors_manual/user_support.rst:90
msgid "Onion skin issues"
msgstr "Questões com a 'pele de cebola'"

#: ../../contributors_manual/user_support.rst:92
msgid ""
"The great majority of issues with onion skin are just user errors, not bugs. "
"Nonetheless, you need to find out why it happens and direct the user how to "
"use onion skin properly."
msgstr ""
"A grande maioria dos problemas com a 'pele de cebola' são erros do "
"utilizador, não do programa. Contudo, é necessário perceber porque acontecem "
"e aconselhar o utilizador a usar adequadamente a 'pele de cebola'."

#: ../../contributors_manual/user_support.rst:96
msgid "Crash"
msgstr "Estoiro"

#: ../../contributors_manual/user_support.rst:98
msgid ""
"In case of crash try to determine if the problem is known, if not, instruct "
"user to create a bug report (or create it yourself) with following "
"information:"
msgstr ""
"No caso de um estoiro, tente determinar se o problema é conhecido; caso "
"contrário, diga ao utilizador para criar um relatório de erros (ou crie-o "
"você mesmo) com as seguintes informações:"

#: ../../contributors_manual/user_support.rst:100
msgid "What happened, what was being done just before the crash."
msgstr "O que aconteceu e o que estava a acontecer antes do estoiro."

#: ../../contributors_manual/user_support.rst:102
msgid ""
"Is it possible to reproduce (repeat)? If yes, provide a step-by-step "
"instruction to get the crash."
msgstr ""
"É possível reproduzi-lo (repetir)? Em caso afirmativo, indique instruções "
"passo-a-passo para chegar ao estoiro."

#: ../../contributors_manual/user_support.rst:104
msgid ""
"Backtrace (crashlog) -- the instruction is here: :ref:`dr_minw`, and the "
"debug symbols can be found in the annoucement of the version of Krita that "
"the user has. But it could be easier to just point the user to `https://"
"download.kde.org/stable/krita <https://download.kde.org/stable/krita>`_."
msgstr ""
"Registo de chamadas (registo do estoiro) -- a instrução está aqui: :ref:"
"`dr_minw` e os símbolos de depuração poderão ser encontrados no anúncio da "
"da versão do Krita que o utilizador possui. Mas poderá ser mais simples "
"apontar o utilizador para `https://download.kde.org/stable/krita <https://"
"download.kde.org/stable/krita>`_."

#: ../../contributors_manual/user_support.rst:108
msgid "Other possible questions with quick solutions"
msgstr "Outras questões possíveis com soluções rápidas"

#: ../../contributors_manual/user_support.rst:110
msgid ""
"When the user has trouble with anything related to preview or display, ask "
"them to change :guilabel:`Canvas Graphics Acceleration` in :menuselection:"
"`Settings --> Configure Krita --> Display`."
msgstr ""
"Quando o utilizador tem problemas com tudo o que é relacionado com a "
"antevisão ou a visualização, peça-lhes para mudar a :guilabel:`Aceleração "
"Gráfica da Área de Desenho` em :menuselection:`Configuração --> Configurar o "
"Krita --> Visualização`."

#: ../../contributors_manual/user_support.rst:116
msgid ""
"When the user has any weird issue, something you've never heard about, ask "
"them to reset the configuration: :ref:`faq_reset_krita_configuration`."
msgstr ""
"Quando o utilizador tiver algum problema estranho, algo com que nunca se "
"tenha deparado, peça-lhes para repor a configuração predefinida: :ref:"
"`faq_reset_krita_configuration`."

#: ../../contributors_manual/user_support.rst:120
msgid "Advices for supporters"
msgstr "Conselhos de suporte"

#: ../../contributors_manual/user_support.rst:122
msgid ""
"If you don't understand the question, ask for clarification -- asking for a "
"screen recording or a screenshot is perfectly fine."
msgstr ""
"Se não compreender a questão, peça algum esclarecimento -- peça uma gravação "
"do ecrã ou uma captura do ecrã, o que será perfeito."

#: ../../contributors_manual/user_support.rst:124
msgid ""
"If you don't know the solution but you know what information will be needed "
"to investigate the issue further, don't hesitate to ask. Other supporters "
"may know the answer, but have too little time to move the user through the "
"whole process, so you're helping a lot just by asking for additional "
"information. This is very much true in case of tablet issues, for example."
msgstr ""
"Se não souber a solução, mas souber qual a informação que é necessária para "
"investigar mais o assunto, não hesite em perguntar. Outros técnicos de "
"suporte poderão saber a resposta, mas terem pouco tempo para acompanhar o "
"utilizador por todo o processo, pelo que já estará a ajudar bastante se "
"pedir informações adicionais. Isto é bastante válido no caso de problemas "
"com tabletes, por exemplo."

#: ../../contributors_manual/user_support.rst:126
msgid ""
"If you don't know the answer/solution and the question looks abandoned by "
"other supporters, you can always ask for help on Krita IRC channel. It's "
"#krita on freenote.net: :ref:`the_krita_community`."
msgstr ""
"Se não souber a resposta/solução e a pergunta parecer abandonada pelos "
"outros técnicos de suporte, pode sempre pedir ajuda no canal de IRC do "
"Krita. É o #krita no freenote.net: :ref:`the_krita_community`."

#: ../../contributors_manual/user_support.rst:128
msgid ""
"Explain steps the user needs to make clearly, for example if you need them "
"to change something in settings, clearly state the whole path of buttons and "
"tabs to get there."
msgstr ""
"Explique os passos que o utilizador precisa para clarificar; por exemplo, se "
"precisa que eles mudem alguma coisa na configuração, indicando o caminho até "
"chegar aos botões ou páginas respectivas."

#: ../../contributors_manual/user_support.rst:130
msgid ""
"Instead of :menuselection:`Settings --> Configure Krita` use just :"
"menuselection:`Configure Krita` -- it's easy enough to find and Mac users "
"(where you need to select :menuselection:`Krita --> Settings`) won't get "
"confused."
msgstr ""
"Em vez do :menuselection:`Configuração -> Configurar o Krita` use apenas :"
"menuselection:`Configurar o Krita` -- é simples o suficiente de descobrir e "
"os utilizadores do Mac (onde precisa de seleccionar :menuselection:`Krita -> "
"Configuração`) não ficarão confundidos."

#: ../../contributors_manual/user_support.rst:132
msgid ""
"If you ask for an image, mention usage of `Imgur <https://imgur.com>`_ or "
"`Pasteboard <https://pasteboard.co>`_, otherwise Reddit users might create a "
"new post with this image instead of including it to the old conversation."
msgstr ""
"Se pedir uma imagem, mencione a utilização do `Imgur <https://imgur.com>`_ "
"ou do `Pasteboard <https://pasteboard.co>`_; caso contrário os utilizadores "
"do Reddit poderão criar uma nova publicação com esta imagem em alternativa, "
"incluindo-a na conversação antiga."

#: ../../contributors_manual/user_support.rst:134
msgid ""
"If you want to quickly answer someone, just link to the appropriate place in "
"this manual page -- you can click on the little link icon next to the "
"section or subsection title and give the link to the user so they for "
"example know what information about their tablet issue you need."
msgstr ""
"Se quiser responder rapidamente a alguém, basta fazer referência ao local "
"apropriado nesta página do manual -- poderá carregar no pequeno ícone a "
"seguir ao título da secção ou sub-secção e passe a ligação ao utilizador, "
"para que eles possam saber a informação da tablete deles que você precisa."

#: ../../contributors_manual/user_support.rst:136
msgid ""
"If the user access the internet from the country or a workplace with some of "
"the websites blocked (like imgur.com or pastebin.com), here is a list of "
"alternatives that works:"
msgstr ""
"Se o utilizador aceder à Internet a partir do campo ou no emprego, onde "
"algumas das páginas possam estar bloqueadas (como o imgur.com ou o pastebin."
"com), aqui está uma lista das alternativas que resultam:"

#: ../../contributors_manual/user_support.rst:138
msgid "Images (e.g. screenshots): `Pasteboard <https://pasteboard.co>`_"
msgstr ""
"Imagens (p.ex. capturas de ecrã): `Pasteboard <https://pasteboard.co>`_"

#: ../../contributors_manual/user_support.rst:140
msgid ""
"Text only: `BPaste <https://bpaste.net>`_, `paste.ubuntu.org.cn <paste."
"ubuntu.org.cn>`_, `paste.fedoraproject.org <https://paste.fedoraproject.org/"
">`_ or `https://invent.kde.org/dashboard/snippets (needs KDE Identity) "
"<https://invent.kde.org/dashboard/snippets>`_."
msgstr ""
"Apenas texto: `BPaste <https://bpaste.net>`, `paste.ubuntu.org.cn <paste."
"ubuntu.org.cn>`, `paste.fedoraproject.org <https://paste.fedoraproject.org/"
">`, `https://invent.kde.org/dashboard/snippets (precisa do KDE Identity) "
"<https://invent.kde.org/dashboard/snippets>`_."

#: ../../contributors_manual/user_support.rst:142
msgid ""
"``.kra`` and other formats: by mail? Or encode the file using `base64` "
"command on Linux, send by mail or on Pastebin, then decode using the same "
"command."
msgstr ""
"``.kra`` e outros formatos: por e-mail? Ou codifique o ficheiro com o "
"comando `base64` no Linux, envie-o por e-mail ou pelo Pastebin, "
"descodificando-o com o mesmo comando."

#: ../../contributors_manual/user_support.rst:147
msgid ""
"If you ask user to store their log or other data on a website, make sure it "
"stays there long enough for you to get it -- for example bpaste.net stores "
"files by default only for a day! And you can extend it only to one week."
msgstr ""
"Se pedir ao utilizador para guardar os registos deles ou outros dados numa "
"página Web, certifique-se que fica lá o tempo suficiente para você aceder à "
"mesma -- por exemplo o bpaste.net guarda os ficheiros por omissão apenas "
"durante um dia! Poderá à mesma estender o período para uma semana."

#: ../../contributors_manual/user_support.rst:149
msgid ""
"Make sure they don't post their personal data. Tablet Tester log is safe, "
"log from the :menuselection:`Help -> Show system information for bug "
"reports` might not be that safe. Maybe you could ask them to send it to you "
"by mail?"
msgstr ""
"Certifique-se que eles não publicam os dados pessoais deles. O registo do "
"Teste da Tablete é suficientemente seguro, enquanto o registo em :"
"menuselection:`Ajuda -> Mostrar a informação do sistema para os relatórios "
"de erros` poderá não ser tão seguro. Talvez lhes possa pedir para lhe "
"enviarem esses dados por e-mail?"
