# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-17 17:27+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: optionsize optionshapedyna optionairbrush\n"
"X-POFile-SpellExtra: optionopacitynflow icons etiver image optionrotation\n"
"X-POFile-SpellExtra: guilabel px optionbrushtip optionsprayshape\n"
"X-POFile-SpellExtra: optioncolorspray blendingmodes optionsprayarea images\n"
"X-POFile-SpellExtra: spraybrush ref\n"

#: ../../<generated>:1
msgid "Mix with background color."
msgstr "Mistura com a cor de fundo."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:1
msgid "The Spray Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis de Spray."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:16
msgid "Spray Brush Engine"
msgstr "Motor de Pincéis de Spray"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Airbrush"
msgstr "Pincel de 'spray'"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:19
msgid ".. image:: images/icons/spraybrush.svg"
msgstr ".. image:: images/icons/spraybrush.svg"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:20
msgid "A brush that can spray particles around in its brush area."
msgstr ""
"Um pincel que poderá pulverizar partículas à volta da área do seu pincel."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:23
msgid "Options"
msgstr "Opções"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:25
msgid ":ref:`option_spray_area`"
msgstr ":ref:`option_spray_area`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:26
msgid ":ref:`option_spray_shape`"
msgstr ":ref:`option_spray_shape`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:27
msgid ":ref:`option_brush_tip` (Used as particle if spray shape is not active)"
msgstr ""
":ref:`option_brush_tip` (Usada como partícula se a forma do spray não "
"estiver seleccionada)"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:28
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:29
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:30
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:31
msgid ":ref:`option_shape_dyna`"
msgstr ":ref:`option_shape_dyna`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:32
msgid ":ref:`option_color_spray`"
msgstr ":ref:`option_color_spray`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:34
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:39
msgid "Spray Area"
msgstr "Área do 'Spray'"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:41
msgid "The area in which the particles are sprayed."
msgstr "A área na qual são pulverizadas as partículas."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:43
msgid "Diameter"
msgstr "Diâmetro"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:44
msgid "The size of the area."
msgstr "O tamanho da área."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:45
msgid "Aspect Ratio"
msgstr "Proporções"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:46
msgid "It's aspect ratio: 1.0 is fully circular."
msgstr "A sua proporção de dimensões: o 1,0 é completamente circular."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:47
msgid "Angle"
msgstr "Ângulo"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:48
msgid ""
"The angle of the spray size: works nice with aspect ratios other than 1.0."
msgstr ""
"O ângulo do tamanho do pincel: funciona bem com proporções de tamanho "
"diferentes de 1,0."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:49
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:50
msgid "Scales the diameter up."
msgstr "Aumenta a escala do diâmetro."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Spacing"
msgstr "Espaço"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Increases the spacing of the diameter's spray."
msgstr "Aumenta o espaçamento dentro do diâmetro do 'spray'."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:55
msgid "Particles"
msgstr "Partículas"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:57
msgid "Count"
msgstr "Quantidade"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:58
msgid "Use a specified amount of particles."
msgstr "Usa um número de partículas bem definido."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:59
msgid "Density"
msgstr "Densidade"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:60
msgid "Use a % amount of particles."
msgstr "Uma quantidade em percentagem de partículas."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:61
msgid "Jitter Movement"
msgstr "Movimento Súbito"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:62
msgid "Jitters the spray area around for extra randomness."
msgstr ""
"Agita a área do 'spray' à volta, para obter alguma aleatoriedade extra."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid "Gaussian Distribution"
msgstr "Distribuição Gaussiana"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid ""
"Focuses the particles to paint in the center instead of evenly random over "
"the spray area."
msgstr ""
"Foca as partículas para se pintarem ao centro em vez de estarem dispersas "
"aleatoriamente pela área do 'spray'."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:69
msgid "Spray Shape"
msgstr "Forma do 'Spray'"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:71
msgid ""
"If activated, this will generate a special particle. If not, the brush-tip "
"will be the particle."
msgstr ""
"Se etiver activa, irá gerar uma partícula especial. Caso contrário, a ponta "
"do pincel será a partícula."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:74
msgid "Can be..."
msgstr "Pode ser..."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:76
msgid "Ellipse"
msgstr "Elipse"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:77
msgid "Rectangle"
msgstr "Rectângulo"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:78
msgid "Anti-aliased Pixel"
msgstr "Pixel Suavizado"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:79
msgid "Pixel"
msgstr "Pixel"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Shape"
msgstr "Forma"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Image"
msgstr "Imagem"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:82
msgid "Width & Height"
msgstr "Largura & Altura"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:83
msgid "Decides the width and height of the particle."
msgstr "Decide a largura e a altura da partícula."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:84
msgid "Proportional"
msgstr "Proporcional"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:85
msgid "Locks Width & Height to be the same."
msgstr "Bloqueia a Largura & Altura para ficarem iguais."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Texture"
msgstr "Textura"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Allows you to pick an image for the :guilabel:`Image shape`."
msgstr "Permite-lhe escolher uma imagem para a :guilabel:`Forma da imagem`."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:92
msgid "Shape Dynamics"
msgstr "Parâmetros Dinâmicos da Forma"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:94
msgid "Random Size"
msgstr "Tamanho Aleatório"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:95
msgid ""
"Randomizes the particle size between 1x1 px and the given size of the "
"particle in brush-tip or spray shape."
msgstr ""
"Gera um tamanho aleatório da partícula entre 1x1 px e o tamanho indicado da "
"partícula na ponta do pincel ou na forma do 'spray'."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:96
msgid "Fixed Rotation"
msgstr "Rotação Fixa"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:97
msgid "Gives a fixed rotation to the particle to work from."
msgstr "Atribui uma rotação fixa à partícula quando a usar."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:98
msgid "Randomized Rotation"
msgstr "Rotação Aleatória"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:99
msgid "Randomizes the rotation."
msgstr "Atribui um ângulo de rotação aleatório."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:100
msgid "Follow Cursor Weight"
msgstr "Seguir o Peso do Cursor"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:101
msgid ""
"How much the pressure affects the rotation of the particles. At 1.0 and high "
"pressure it'll seem as if the particles are exploding from the middle."
msgstr ""
"Quanto é que a pressão afecta a rotação das partículas. Em 1,0 e com uma "
"pressão elevada, parecerá que as partículas explodiram a partir do centro."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "Angle Weight"
msgstr "Peso do Ângulo"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "How much the spray area angle affects the particle angle."
msgstr "Como é que o ângulo da área do 'spray' afecta o ângulo da partícula."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:108
msgid "Color Options"
msgstr "Opções de Cores"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:110
msgid "Random HSV"
msgstr "HSV Aleatório"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:111
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""
"Atribui valores aleatórios ao HSV com a potência das barras. Quanto maiores "
"os valores, mais a cor se irá desviar da cor principal, com a direcção a "
"indicar o sentido horário ou anti-horário."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:112
msgid "Random Opacity"
msgstr "Opacidade Aleatória"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:113
msgid "Randomizes the opacity."
msgstr "Atribui valores aleatórios à opacidade."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:114
msgid "Color Per Particle"
msgstr "Cor por Partícula"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:115
msgid "Has the color options be per particle instead of area."
msgstr ""
"Tem as opções de cores a serem aplicadas por partícula em vez de ser por "
"área."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:116
msgid "Sample Input Layer."
msgstr "Camada de Entrada de Exemplo."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:117
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr ""
"Irá usar a camada subjacente como referência para as cores em vez de usar a "
"cor principal."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:118
msgid "Fill Background"
msgstr "Preencher o Fundo"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:119
msgid "Fills the area before drawing the particles with the background color."
msgstr "Preenche a área antes de desenhar as partículas com a cor de fundo."

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:121
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr ""
"Atribui à partícula uma cor aleatória entre a cor principal/de entrada/HSV "
"aleatório e a cor de fundo."
