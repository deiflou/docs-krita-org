# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-07 15:49+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Rename composition"
msgstr "Compositie hernoemen"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: muisrechts"

#: ../../reference_manual/dockers/compositions.rst:1
msgid "Overview of the compositions docker."
msgstr "Overzicht van de vastzetter Composities."

#: ../../reference_manual/dockers/compositions.rst:12
#: ../../reference_manual/dockers/compositions.rst:17
msgid "Compositions"
msgstr "Composities"

#: ../../reference_manual/dockers/compositions.rst:19
msgid ""
"The compositions docker allows you to save the configurations of your layers "
"being visible and invisible, allowing you to save several configurations of "
"your layers."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:22
msgid ".. image:: images/dockers/Composition-docker.png"
msgstr ".. image:: images/dockers/Composition-docker.png"

#: ../../reference_manual/dockers/compositions.rst:24
msgid "Adding new compositions"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:24
msgid ""
"You do this by setting your layers as you wish, then pressing the plus sign. "
"If you had a word in the text-box to the left, this will be the name of your "
"new composition."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:26
msgid "Activating composition"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:27
msgid "Double-click the composition name to switch to that composition."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:28
msgid "Removing compositions"
msgstr "Composities verwijderen"

#: ../../reference_manual/dockers/compositions.rst:29
msgid "The minus sign. Select a composition, and hit this button to remove it."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:30
msgid "Exporting compositions"
msgstr "Composities exporteren"

#: ../../reference_manual/dockers/compositions.rst:31
msgid "The file sign. Will export all checked compositions."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:32
msgid "Updating compositions"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:33
msgid ""
"|mouseright| a composition to overwrite it with the current configuration."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:35
msgid "|mouseright| a composition to rename it."
msgstr ""
