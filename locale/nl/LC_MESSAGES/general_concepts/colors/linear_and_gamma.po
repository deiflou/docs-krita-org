# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
# Lo Fernchu <lofernchu@zonnet.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-19 10:32+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/trc_gray_gradients.svg"
msgstr ".. image:: images/color_category/trc_gray_gradients.svg"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/Basicreading3trcsv2.svg"
msgstr ".. image:: images/color_category/Basicreading3trcsv2.svg"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/red_green_mixes_trc.svg"
msgstr ".. image:: images/color_category/red_green_mixes_trc.svg"

#: ../../general_concepts/colors/linear_and_gamma.rst:None
msgid ".. image:: images/color_category/3trcsresult.png"
msgstr ".. image:: images/color_category/3trcsresult.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:1
msgid "The effect of gamma and linear."
msgstr "Het effect van gamma en lineair."

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Gamma"
msgstr "Gamma"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Linear Color Space"
msgstr "Lineaire kleurruimte"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Linear"
msgstr "Lineair"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Tone Response curve"
msgstr "Responskromme van toon"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "EOTF"
msgstr "EOTF"

#: ../../general_concepts/colors/linear_and_gamma.rst:11
msgid "Transfer Curve"
msgstr "Overdrachtskromme"

#: ../../general_concepts/colors/linear_and_gamma.rst:17
msgid "Gamma and Linear"
msgstr "Gamma en lineair"

#: ../../general_concepts/colors/linear_and_gamma.rst:19
msgid ""
"Now, the situation we talk about when talking theory is what we would call "
"'linear'. Each step of brightness is the same value. Our eyes do not "
"perceive linearly. Rather, we find it more easy to distinguish between "
"darker grays than we do between lighter grays."
msgstr ""
"De situatie waarover we nu praten, theoretisch gesproken, is wat we "
"'lineair' zouden noemen. Elke stap in helderheid heeft dezelfde waarde. Onze "
"ogen zien niet lineair. We vinden het echter gemakkelijker het onderscheid "
"te zien tussen donkere grijzen dan tussen lichtere grijzen."

#: ../../general_concepts/colors/linear_and_gamma.rst:22
msgid ""
"As humans are the ones using computers, we have made it so that computers "
"will give more room to darker values in the coordinate system of the image. "
"We call this 'gamma-encoding', because it is applying a gamma function to "
"the TRC or transfer function of an image. The TRC in this case being the "
"Tone Response Curve or Tone Reproduction Curve or Transfer function (because "
"color management specialists hate themselves), which tells your computer or "
"printer how much color corresponds to a certain value."
msgstr ""
"Omdat het mensen zijn die computers gebruiken, hebben we ze zo gemaakt dat "
"computers meer ruimte geven aan de donkere waarden in het coördinatensysteem "
"van de afbeelding. We noemen dit 'gamma-coderen', omdat dit een gamma "
"functie toepast op de TRC of overdracht functie van een afbeelding. De TRC "
"in dit geval is de Toon Response Curve of Toon Reproductie Curve of Transfer "
"functie (omdat kleur beheer specialisten zichzelf haten), wat uw computer of "
"printer vertelt hoeveel kleur bij een bepaalde waarde hoort."

#: ../../general_concepts/colors/linear_and_gamma.rst:28
msgid ".. image:: images/color_category/Pepper_tonecurves.png"
msgstr " .. image:: images/en/color_category/Pepper_tonecurves.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:28
msgid ""
"One of the most common issues people have with Krita's color management is "
"the assigning of the right colorspace to the encoded TRC. Above, the center "
"Pepper is the right one, where the encoded and assigned TRC are the same. To "
"the left we have a Pepper encoded in sRGB, but assigned a linear profile, "
"and to the right we have a Pepper encoded with a linear TRC and assigned an "
"sRGB TRC. Image from `Pepper & Carrot <https://www.peppercarrot.com/>`_."
msgstr ""
"Een van de meest voorkomende problemen die mensen met Krita's kleurbeheer "
"hebben is het toewijzen van de juiste kleurruimte aan de toegepaste TRC. "
"Hierboven is de middelste Pepper de juiste, omdat de ingestelde en de "
"toegepaste TRC dezelfde zijn. Aan de linkerzijde hebben we een Pepper met "
"een sRGB toegepast, maar waar een lineair profiel is toegewezen, en aan de "
"rechterzijde hebben we een Pepper waar een lineaire TRC is toegepast en waar "
"een sRGB TRC is toegewezen. Afbeelding komt uit `Pepper & Carrot <https://"
"www.peppercarrot.com/>`_"

#: ../../general_concepts/colors/linear_and_gamma.rst:30
msgid ""
"The following table shows how there's a lot of space being used by lighter "
"values in a linear space compared to the default sRGB TRC of our modern "
"computers and other TRCs available in our delivered profiles:"
msgstr ""
"De volgende tabel laat zien dat in een lineaire ruimte veel ruimte wordt "
"gebruikt voor de lichtere waarden in vergelijking met de standaard sRGB TRC "
"van onze moderne computers en andere in onze meegeleverde profielen "
"beschikbare TRCs:"

#: ../../general_concepts/colors/linear_and_gamma.rst:35
msgid ""
"If you look at linear of Rec. 709 TRCs, you can see there's quite a jump "
"between the darker shades and the lighter shades, while if we look at the "
"Lab L* TRC or the sRGB TRC, which seem more evenly spaced. This is due to "
"our eyes' sensitivity to darker values. This also means that if you do not "
"have enough bit depth, an image in a linear space will look as if it has "
"ugly banding. Hence why, when we make images for viewing on a screen, we "
"always use something like the Lab L\\*, sRGB or Gamma 2.2 TRCs to encode the "
"image with."
msgstr ""
"Als u de lijn van Rec 709 TRCs kijkt, dan kunt u zien dat er flinke sprong "
"is tussen de donkerder tinten en de lichtere tinten, terwijl als we kijken "
"naar de Lab L* TRC of de sRGB TRC, dan lijkt het meer evenredig verdeelt. "
"Dit is zo omdat onze ogen gevoeliger zijn voor de donkerder waarden. Dit "
"houdt ook in dat als u niet genoeg bit diepte heeft, dan lijkt of een "
"afbeelding met een lineaire ruimte er uit ziet alsof het lelijke kleurbanden "
"heeft. Dit is de reden dat we bij het maken van afbeeldingen die op een "
"scherm worden bekeken, dat we altijd zoiets als de Lab L\\*, sRGB of Gamma "
"2.2 TRCs gebruiken voor het coderen."

#: ../../general_concepts/colors/linear_and_gamma.rst:38
msgid ""
"However, this modification to give more space to darker values does lead to "
"wonky color maths when mixing the colors."
msgstr ""
"Echter, deze wijziging om meer ruimte te geven aan de donkere waarden geeft "
"ingewikkeldere berekeningen bij het mengen van kleuren."

#: ../../general_concepts/colors/linear_and_gamma.rst:40
msgid "We can see this with the following experiment:"
msgstr "We kunnen dit zien met het volgende experiment:"

#: ../../general_concepts/colors/linear_and_gamma.rst:46
msgid ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_1.png"
msgstr ""
" .. image:: images/en/color_category/Krita_2_9_colormanagement_blending_1.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:46
msgid ""
"**Left:** Colored circles blurred in a regular sRGB space. **Right:** "
"Colored circles blurred in a linear space."
msgstr ""
"**Links:** gekleurde cirkels vervaagd in een reguliere sRGB ruimte. **Rechts:"
"** gekleurde cirkels vervaagd in een lineaire ruimte."

#: ../../general_concepts/colors/linear_and_gamma.rst:48
msgid ""
"Colored circles, half blurred. In a gamma-corrected environment, this gives "
"an odd black border. In a linear environment, this gives us a nice gradation."
msgstr ""
"Gekleurde cirkels, half geblurred. In een gamma-gecorrigeerde omgeving, "
"geeft dit een rare zwarte rand. In een lineaire omgeving, geeft dit ons een "
"mooi kleurverloop."

#: ../../general_concepts/colors/linear_and_gamma.rst:50
msgid "This also counts for Krita's color smudge brush:"
msgstr "Dit geldt ook voor de kleur uitsmerende penseel van Krita:"

#: ../../general_concepts/colors/linear_and_gamma.rst:56
msgid ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_2.png"
msgstr ""
".. image:: images/color_category/Krita_2_9_colormanagement_blending_2.png"

#: ../../general_concepts/colors/linear_and_gamma.rst:56
msgid ""
"That's right, the 'muddying' of colors as is a common complaint by digital "
"painters everywhere, is in fact, a gamma-corrected colorspace mucking up "
"your colors. If you had been working in LAB to avoid this, be sure to try "
"out a linear rgb color space."
msgstr ""
"Dat klopt, het 'modderige' van kleuren dat een veel voorkomende klacht is "
"van veel digitale tekenaars, is in feite, een gamma-gecorrigeerde "
"kleurruimte dat uw kleuren modderig maakt. Als u in LAB werkte om dit te "
"vermijden, probeer dan eens een lineaire rgb kleurruimte."

#: ../../general_concepts/colors/linear_and_gamma.rst:59
msgid "What is happening under the hood"
msgstr "Wat gebeurt er onder de motorkap"

#: ../../general_concepts/colors/linear_and_gamma.rst:62
msgid "Imagine we want to mix red and green."
msgstr "Stel je voor datwe rood en groen willen mengen."

#: ../../general_concepts/colors/linear_and_gamma.rst:64
msgid ""
"First, we would need the color coordinates of red and green inside our color "
"space's color model. So, that'd be..."
msgstr ""
"Ten eerste hebben we de kleur-coördinaten van rood en groen in ons "
"kleurmodel nodig. Dat is dus..."

#: ../../general_concepts/colors/linear_and_gamma.rst:67
msgid "Color"
msgstr "Kleur"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:76
#: ../../general_concepts/colors/linear_and_gamma.rst:78
msgid "Red"
msgstr "Rood"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:76
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "Green"
msgstr "Groen"

#: ../../general_concepts/colors/linear_and_gamma.rst:67
#: ../../general_concepts/colors/linear_and_gamma.rst:82
msgid "Blue"
msgstr "Blauw"

#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "1.0"
msgstr "1.0"

#: ../../general_concepts/colors/linear_and_gamma.rst:69
#: ../../general_concepts/colors/linear_and_gamma.rst:70
#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
#: ../../general_concepts/colors/linear_and_gamma.rst:82
msgid "0.0"
msgstr "0.0"

#: ../../general_concepts/colors/linear_and_gamma.rst:73
msgid "We then average these coordinates over three mixes:"
msgstr "We nemen dan het gemiddelde van de coördinaten uit de drie mengsels:"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix1"
msgstr "Mengsel1"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix2"
msgstr "Mengsel2"

#: ../../general_concepts/colors/linear_and_gamma.rst:76
msgid "Mix3"
msgstr "Mengsel3"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.75"
msgstr "0.75"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.5"
msgstr "0.5"

#: ../../general_concepts/colors/linear_and_gamma.rst:78
#: ../../general_concepts/colors/linear_and_gamma.rst:80
msgid "0.25"
msgstr "0.25"

#: ../../general_concepts/colors/linear_and_gamma.rst:85
msgid ""
"But to figure out how these colors look on screen, we first put the "
"individual values through the TRC of the color-space we're working with:"
msgstr ""
"Maar om uit te vinden hoe deze kleuren op het scherm eruit zien, moeten we "
"eerst de individuele waarden door de TRC van de kleurruimte halen waar we "
"mee werken:"

#: ../../general_concepts/colors/linear_and_gamma.rst:93
msgid ""
"Then we fill in the values into the correct spot. Compare these to the "
"values of the mixture table above!"
msgstr ""
"Dan vullen we de waarden in op de juiste locatie. Vergelijk deze met de "
"waarden van de meng-tabel hierboven!"

#: ../../general_concepts/colors/linear_and_gamma.rst:99
msgid ""
"And this is why color mixtures are lighter and softer in linear space. "
"Linear space is more physically correct, but sRGB is more efficient in terms "
"of space, so hence why many images have an sRGB TRC encoded into them. In "
"case this still doesn't make sense: *sRGB gives largely* **darker** *values "
"than linear space for the same coordinates*."
msgstr ""
"En dit is waarom kleurenmengsels lichter en zachter zijn in een lineaire "
"ruimte. Lineaire ruimte is fysiek meer correct, maar sRGB is meer efficient "
"in qua ruimte, dit is dus waarom veel afbeeldingen een sRGB TRC ingebed "
"hebben. In dit geval maakt dit niks uit: *sRGB geeft voor dezelfde "
"coördinaten grotendeels* **donkerder** *waarden dan een lineaire ruimte voor "
"dezelfde coördinaten*."

#: ../../general_concepts/colors/linear_and_gamma.rst:102
msgid ""
"So different TRCs give different mixes between colors, in the following "
"example, every set of gradients is in order a mix using linear TRC, a mix "
"using sRGB TRC and a mix using Lab L* TRC."
msgstr ""
"Verschillende TRCs geven dus verschillende mengsels tussen kleuren, in het "
"volgende voorbeeld is elke set kleurverlopen in volgorde een mengsel dat een "
"lineaire TRC gebruikt, een mengsel dat sRGB TRC gebruikt en een mengsel dat "
"Lab L* TRC gebruikt."

#: ../../general_concepts/colors/linear_and_gamma.rst:110
msgid ""
"So, you might be asking, how do I tick this option? Is it in the settings "
"somewhere? The answer is that we have several ICC profiles that can be used "
"for this kind of work:"
msgstr ""
"U zal zich dus afvragen hoe u dit probleem oplost? Is het ergens in de "
"instellingen te vinden? Het antwoord is dat we meerdere ICC profielen hebben "
"die u voor dit soort werk kunt gebruiken:"

#: ../../general_concepts/colors/linear_and_gamma.rst:112
msgid "scRGB (linear)"
msgstr "scRGB (linear)"

#: ../../general_concepts/colors/linear_and_gamma.rst:113
msgid "All 'elle'-profiles ending in 'g10', such as *sRGB-elle-v2-g10.icc*."
msgstr ""
"Alle 'elle'-profielen die eindigen op 'g10', zoals *sRGB-elle-v2-g10.icc*."

#: ../../general_concepts/colors/linear_and_gamma.rst:115
msgid ""
"In fact, in all the 'elle'-profiles, the last number indicates the gamma. "
"1.0 is linear, higher is gamma-corrected and 'srgbtrc' is a special gamma "
"correction for the original sRGB profile."
msgstr ""
"Eigenlijk geeft bij alle 'elle'-profielen, het laatste nummer de gamma aan. "
"1.0 is lineair, groter is gamma-gecorrigeerd en 'srgbtrc' is een speciale "
"gamma correctie op het originele sRGB profiel."

#: ../../general_concepts/colors/linear_and_gamma.rst:117
msgid ""
"If you use the color space browser, you can tell the TRC from the 'estimated "
"gamma'(if it's 1.0, it's linear), or from the TRC widget in Krita 3.0, which "
"looks exactly like the curve graphs above."
msgstr ""
"Als u de browser voor kleurruimtes gebruikt, dan kunt u de TRC te weten "
"komen door de 'geschatte gamma'(als het 1.0 is, dan is het lineair), of in "
"het TRC-widget in Krita 3.0, wat er precies zo uit ziet als grafieken "
"hierboven."

#: ../../general_concepts/colors/linear_and_gamma.rst:119
msgid ""
"Even if you do not paint much, but are for example making textures for a "
"videogame or rendering, using a linear space is very beneficial and will "
"speed up the renderer a little, for it won't have to convert images on its "
"own."
msgstr ""
"Zelfs als u niet veel tekent, maar bijvoorbeeld texturen voor een "
"videospelletjes of rendering maakt, dan kan een lineaire ruimte erg handig "
"zijn en zal het renderen een beetje sneller gaan, omdat het de afbeeldingen "
"niet zelf hoeft te converteren."

#: ../../general_concepts/colors/linear_and_gamma.rst:121
msgid ""
"The downside of linear space is of course that white seems very overpowered "
"when mixing with black, because in a linear space, light grays get more "
"room. In the end, while linear space is physically correct, and a boon to "
"work in when you are dealing with physically correct renderers for "
"videogames and raytracing, Krita is a tool and no-one will hunt you down for "
"preferring the dark mixing of the sRGB TRC."
msgstr ""
"Het nadeel van een lineaire ruimte is natuurlijk dat het wit erg krachtig is "
"bij het mengen met zwart, omdat in een lineaire ruimte, lichte grijzen meer "
"ruimte krijgt. Tenslotte, terwijl lineaire ruimte fysiek correct is, en "
"verplicht is om mee te werken als u met fysiek correcte renderers voor "
"videospelletjes en raytracing werkt, Krita is maar een stuk gereedschap en "
"niemand zal u kwalijk nemen als u de voorkeur geeft het donkere mengsel van "
"een sRGB TRC."
