# Translation of docs_krita_org_reference_manual___dockers___add_shape.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___add_shape\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/dockers/add_shape.rst:1
msgid "The add shape docker."
msgstr "Бічна панель додавання форм."

#: ../../reference_manual/dockers/add_shape.rst:15
msgid "Add Shape"
msgstr "Додавання форми"

#: ../../reference_manual/dockers/add_shape.rst:18
msgid ".. image:: images/dockers/Krita_Add_Shape_Docker.png"
msgstr ".. image:: images/dockers/Krita_Add_Shape_Docker.png"

#: ../../reference_manual/dockers/add_shape.rst:19
msgid "A docker for adding KOffice shapes to a Vector Layers."
msgstr "Бічна панель для додавання форм KOffice на векторні шари."

#: ../../reference_manual/dockers/add_shape.rst:23
msgid "This got removed in 4.0, the :ref:`vector_library_docker` replacing it."
msgstr "Вилучено у версії 4.0, замінено на :ref:`vector_library_docker`."
