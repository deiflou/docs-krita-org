# Translation of docs_krita_org_tutorials___krita-brush-tips___bokeh-brush.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips___bokeh-"
"brush\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:12+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_01.png\n"
"   :alt: krita bokeh brush setup background"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_01.png\n"
"   :alt: Налаштовування тла пензля боке у krita"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_02.png\n"
"   :alt: Krita bokeh brush tips scatter settings"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_02.png\n"
"   :alt: Параметри розсіювання кінчика пензля боке у Krita"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_03.png\n"
"   :alt: Choosing the brush tip for the bokeh effect"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_03.png\n"
"   :alt: Вибір кінчика пензля для ефекту боке"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_04.png\n"
"   :alt: paint the bokeh circles on the background"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_04.png\n"
"   :alt: Малювання кіл боке у тлі"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:1
msgid "Creating bokeh effect with the help of some simple brush tip."
msgstr "Створення ефекту боке за допомогою простого кінчика пензля."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:13
msgid "Brush Tips: Bokeh"
msgstr "Кінчики пензлів: Боке"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:16
msgid "Question"
msgstr "Питання"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:18
msgid "How do you do bokeh effects?"
msgstr "Як створити ефект боке?"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:20
msgid "First, blur your image with the Lens Blur to roughly 50 pixels."
msgstr ""
"Спочатку, виконайте розмивання вашого зображення ефектом :guilabel:"
"`Розмивання об'єктивом` із параметром приблизно 50 пікселів."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:26
msgid "Take smudge_textured, add scattering, turn off tablet input."
msgstr ""
"Візьміть smudge_textured, додайте розсіювання, вимкніть введення з планшета."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:31
msgid ""
"Change the brush-tip to ‘Bokeh’ and check ‘overlay’ (you will want to play "
"with the spacing as well)."
msgstr ""
"Змініть кінчик пензля на «Bokey» і позначте пункт «Накладка» (варто трохи "
"погратися із інтервалом)."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:36
msgid ""
"Then make a new layer over your drawing, set that to ‘lighter color’ (it’s "
"under lighter category) and painter over it with you brush."
msgstr ""
"Далі, створіть шар над вашим малюнком, встановіть для нього «світліший "
"колір» (у категорії освітлення) і малюйте на ньому вашим пензлем."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:41
msgid ""
"Overlay mode on the smudge brush allows you to sample all the layers, and "
"the ‘lighter color’ blending mode makes sure that the Bokeh circles only "
"show up when they are a lighter color than the original pixels underneath. "
"You can further modify this brush by adding a ‘fuzzy’ sensor to the spacing "
"and size options, changing the brush blending mode to ‘addition’, or by "
"choosing a different brush-tip."
msgstr ""
"Режим накладки для пензля розмазування надає вам змогу працювати із усіма "
"шарами, а режим змішування «Світліший колір» забезпечує появу кіл боке, лише "
"вони світлішого кольору за початкові пікселі під ними. Ви можете додатково "
"модифікувати цей пензель додаванням датчика «нечіткості» до параметрів "
"інтервалів та розмірів, змінивши режим змішування на «Додавання» або "
"вибравши інший кінчик пензля."
