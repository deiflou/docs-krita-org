# translation of docs_krita_org_reference_manual___split_layer.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___split_layer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-05-13 13:12+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Palette to use for naming the layers"
msgstr ""

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/split_layer.rst:1
msgid "The Split Layer functionality in Krita"
msgstr ""

#: ../../reference_manual/split_layer.rst:10
msgid "Splitting"
msgstr ""

#: ../../reference_manual/split_layer.rst:10
msgid "Color Islands"
msgstr ""

#: ../../reference_manual/split_layer.rst:15
msgid "Split Layer"
msgstr "Rozdeliť vrstvu"

#: ../../reference_manual/split_layer.rst:16
msgid ""
"Splits a layer according to color. This is useful in combination with the :"
"ref:`colorize_mask` and the :kbd:`R +` |mouseleft| shortcut to select layers "
"at the cursor."
msgstr ""

#: ../../reference_manual/split_layer.rst:18
msgid "Put all new layers in a group layer"
msgstr "Vložiť všetky nové vrstvy do skupinovej vrstvy"

#: ../../reference_manual/split_layer.rst:19
msgid "Put all the result layers into a new group."
msgstr ""

#: ../../reference_manual/split_layer.rst:20
msgid "Put every layer in its own, separate group layer"
msgstr "Vložiť všetky vrstvy do vlastnej, samostatnej skupiny vrstiev"

#: ../../reference_manual/split_layer.rst:21
msgid "Put each layer into its own group."
msgstr ""

#: ../../reference_manual/split_layer.rst:22
msgid "Alpha-lock every new layer"
msgstr "Alfa-zamknúť každú novú vrstvu"

#: ../../reference_manual/split_layer.rst:23
msgid "Enable the alpha-lock for each layer so it is easier to color."
msgstr ""

#: ../../reference_manual/split_layer.rst:24
msgid "Hide the original layer"
msgstr "Skryť pôvodnú vrstvu"

#: ../../reference_manual/split_layer.rst:25
msgid ""
"Turns off the visibility on the original layer so you won't get confused."
msgstr ""

#: ../../reference_manual/split_layer.rst:26
#, fuzzy
#| msgid "Sort layers by amount of non-transparent pixels."
msgid "Sort layers by amount of non-transparent pixels"
msgstr "Triediť vrstvy podľa množstva netransparentných bodov"

#: ../../reference_manual/split_layer.rst:27
msgid ""
"This ensures that the layers with large color swathes will end up at the top."
msgstr ""

#: ../../reference_manual/split_layer.rst:28
#, fuzzy
#| msgid "Disregard opacity."
msgid "Disregard opacity"
msgstr "Ignorovať nepriehľadnosť"

#: ../../reference_manual/split_layer.rst:29
msgid "Whether to take into account transparency of a color."
msgstr ""

#: ../../reference_manual/split_layer.rst:30
msgid "Fuzziness"
msgstr "Neostrosť"

#: ../../reference_manual/split_layer.rst:31
msgid ""
"How precise the algorithm should be. The larger the fuzziness, the less "
"precise the algorithm will be. This is necessary for splitting layers with "
"anti-aliasing, because otherwise you would end up with a separate layer for "
"each tiny pixel."
msgstr ""

#: ../../reference_manual/split_layer.rst:33
msgid ""
"Select the palette to use for naming. Krita will compare the main color of a "
"layer to each color in the palette to get the most appropriate name for it."
msgstr ""
