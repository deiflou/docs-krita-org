# Translation of docs_krita_org_reference_manual___tools___measure.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-25 13:15+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:52
msgid ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: toolmeasure"
msgstr ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: eina de mesura"

#: ../../reference_manual/tools/measure.rst:1
msgid "Krita's measure tool reference."
msgstr "Referència de l'eina Mesura del Krita."

#: ../../reference_manual/tools/measure.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/measure.rst:11
msgid "Measure"
msgstr "Mesura"

#: ../../reference_manual/tools/measure.rst:11
msgid "Angle"
msgstr "Angle"

#: ../../reference_manual/tools/measure.rst:11
msgid "Compass"
msgstr "Compàs"

#: ../../reference_manual/tools/measure.rst:16
msgid "Measure Tool"
msgstr "Eina de mesura"

#: ../../reference_manual/tools/measure.rst:18
msgid "|toolmeasure|"
msgstr "|toolmeasure|"

#: ../../reference_manual/tools/measure.rst:20
msgid ""
"This tool is used to measure distances and angles. Click the |mouseleft| to "
"indicate the first endpoint or vertex of the angle, keep the button pressed, "
"drag to the second endpoint and release the button. The results will be "
"shown on the Tool Options docker. You can choose the length units from the "
"drop-down list."
msgstr ""
"Aquesta eina s'utilitza per a mesurar distàncies i angles. Feu |mouseleft| "
"per indicar el primer punt final o vèrtex de l'angle, manteniu premut el "
"botó, arrossegueu fins al segon punt final i deixeu anar el botó. El "
"resultat es mostrarà a l'acoblador Opcions de l'eina. Podreu escollir les "
"unitats de longitud des del desplegable."

#: ../../reference_manual/tools/measure.rst:23
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/measure.rst:25
msgid ""
"The measure tool-options allow you to change between the units used. Unit "
"conversion varies depending on the DPI setting of a document."
msgstr ""
"Les opcions per a la mesura permeten canviar entre les unitats emprades. La "
"conversió d'unitats variarà segons l'ajustament PPP d'un document."
