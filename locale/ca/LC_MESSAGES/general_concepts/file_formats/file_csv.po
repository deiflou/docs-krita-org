# Translation of docs_krita_org_general_concepts___file_formats___file_csv.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:39+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_csv.rst:1
msgid "The CSV file format as exported by Krita."
msgstr "El format de fitxer CSV tal com l'exporta el Krita."

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "*.csv"
msgstr "*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "CSV"
msgstr "CSV"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "Comma Separated Values"
msgstr "Valors separats per comes"

#: ../../general_concepts/file_formats/file_csv.rst:15
msgid "\\*.csv"
msgstr "\\*.csv"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_csv.rst:17
msgid ""
"``.csv`` is the abbreviation for Comma Separated Values. It is an open, "
"plain text spreadsheet format. Since the CSV format is a plain text itself, "
"it is possible to use a spreadsheet program or even a text editor to edit "
"the ``*.csv`` file."
msgstr ""
"El ``.csv`` és l'abreujament de valors separats per comes. És un format de "
"full de càlcul obert i de text pla. Atès que el format CSV és text pla en si "
"mateix, és possible utilitzar un programa de full de càlcul o fins i tot un "
"editor de text per editar el fitxer ``*.csv``."

#: ../../general_concepts/file_formats/file_csv.rst:19
msgid ""
"Krita supports the CSV version used by TVPaint, to transfer layered "
"animation between these two softwares and probably with others, like "
"Blender. This is not an image sequence format, so use the document loading "
"and saving functions in Krita instead of the :guilabel:`Import animation "
"frames` and :guilabel:`Render Animation` menu items."
msgstr ""
"El Krita admet la versió CSV que utilitza el TVPaint per a transferir "
"animació en capes entre aquests dos programaris i, probablement, amb altres "
"com el Blender. No es tracta d'un format de seqüència d'imatges, així que "
"utilitzeu les funcions de càrrega i desament de documents al Krita en "
"comptes dels elements del menú :guilabel:`Importa fotogrames d'animació` i :"
"guilabel:`Renderitza l'animació`."

#: ../../general_concepts/file_formats/file_csv.rst:21
msgid ""
"The format consists of a text file with ``.csv`` extension, together with a "
"folder under the same name and a ``.frames`` extension. The CSV file and the "
"folder must be on the same path location. The text file contains the "
"parameters for the scene, like the field resolution and frame rate, and also "
"contains the exposure sheet for the layers. The folder contains :ref:"
"`file_png` picture files. Unlike image sequences, a key frame instance is "
"only a single file and the exposure sheet links it to one or more frames on "
"the timeline."
msgstr ""
"El format consisteix en un fitxer de text amb l'extensió ``.csv``, juntament "
"amb una carpeta amb el mateix nom i una extensió ``.frames``. El fitxer CSV "
"i la carpeta hauran d'estar a la mateixa ubicació del camí. El fitxer de "
"text conté els paràmetres per a l'escena, com la resolució i la velocitat "
"dels fotogrames del camp, i també conté el full d'exposició per a les capes. "
"La carpeta conté els fitxers d'imatge :ref:`file_png`. A diferència de les "
"seqüències d'imatge, una instància de fotograma clau només serà un únic "
"fitxer i el full d'exposició l'enllaçarà amb un o més fotogrames en la línia "
"de temps."

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid ".. image:: images/Csv_spreadsheet.png"
msgstr ".. image:: images/Csv_spreadsheet.png"

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid "A ``.csv`` file as a spreadsheet in :program:`LibreOffice Calc`."
msgstr ""
"Un fitxer ``.csv`` com a full de càlcul al :program:`LibreOffice Calc`."

#: ../../general_concepts/file_formats/file_csv.rst:28
msgid ""
"Krita can both export and import this format. It is recommended to use 8bit "
"sRGB color space because that's the only color space for :program:`TVPaint`. "
"Layer groups and layer masks are also not supported."
msgstr ""
"El Krita pot exportar i importar aquest format. Es recomana utilitzar "
"l'espai de color sRGB de 8 bits, ja que és l'únic espai de color per al :"
"program:`TVPaint`. Les capes de grup i les màscares de capa no estan admeses."

#: ../../general_concepts/file_formats/file_csv.rst:30
msgid ""
"TVPaint can only export this format by itself. In :program:`TVPaint 11`, use "
"the :guilabel:`Export to...` option of the :guilabel:`File` menu, and on the "
"upcoming :guilabel:`Export footage` window, use the :guilabel:`Clip: Layers "
"structure` tab."
msgstr ""
"El TVPaint només pot exportar aquest format per si mateix. Al :program:"
"`TVPaint 11`, utilitzeu l'opció :guilabel:`Exporta a...` del menú :guilabel:"
"`Fitxer`, i a la finestra :guilabel:`Exporta el metratge` utilitzeu la "
"pestanya :guilabel:`Clip: estructura de les capes`."

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid ".. image:: images/Csv_tvp_csvexport.png"
msgstr ".. image:: images/Csv_tvp_csvexport.png"

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid "Exporting into ``.csv`` in TVPaint."
msgstr "Exportant a ``.csv`` al :program:`TVPaint`."

#: ../../general_concepts/file_formats/file_csv.rst:37
msgid ""
"To import this format back into TVPaint there is a George language script "
"extension. See the \"Packs, Plugins, Third party\" section on the TVPaint "
"community forum for more details and also if you need support for other "
"softwares. Moho/Anime Studio and Blender also have plugins to import this "
"format."
msgstr ""
"Per tornar a importar aquest format al TVPaint hi ha una extensió de script "
"en el llenguatge George. Consulteu la secció Paquets, connectors i terceres "
"parts («Packs, Plugins, Third party») del fòrum de la comunitat del TVPaint "
"per obtenir més informació i també si necessiteu ajuda per a altres "
"programaris. Moho/Anime Studio i Blender també tenen connectors per importar "
"aquest format."

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_csv.rst:42
msgid ""
"`CSV import script for TVPaint <https://forum.tvpaint.com/viewtopic.php?"
"f=26&t=9759>`_."
msgstr ""
"`Script d'importació des de CSV per al TVPaint <https://forum.tvpaint.com/"
"viewtopic.php?f=26&t=9759>`_."

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_csv.rst:43
msgid ""
"`CSV import script for Moho/Anime Studio <https://forum.tvpaint.com/"
"viewtopic.php?f=26&t=10050>`_."
msgstr ""
"`Script d'importació des de CSV per al Moho/Anime Studio <https://forum."
"tvpaint.com/viewtopic.php?f=26&t=10050>`_."

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_csv.rst:44
msgid ""
"`CSV import script for Blender <https://developer.blender.org/T47462>`_."
msgstr ""
"`Script d'importació des de CSV per al Blender <https://developer.blender."
"org/T47462>`_."
